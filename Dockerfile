FROM google/cloud-sdk:debian_component_based

# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get install --no-install-recommends -y mariadb-client && \
    rm -rf /var/lib/apt /var/lib/dpkg

COPY backup_all_databases /usr/local/bin/backup_all_databases

RUN chmod +x /usr/local/bin/backup_all_databases

ENTRYPOINT ["/usr/local/bin/backup_all_databases","-v"]
